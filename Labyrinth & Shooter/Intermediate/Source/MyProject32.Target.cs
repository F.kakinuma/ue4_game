using UnrealBuildTool;

public class MyProject32Target : TargetRules
{
	public MyProject32Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("MyProject32");
	}
}
